package org.handsonknowledge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMicroserviceExampleApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringbootMicroserviceExampleApplication.class, args);
	}
}
