package org.handsonknowledge;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringbootMicroserviceController {
	@GetMapping("/helloWorld")
	public String helloWorld() {
		return "Hello world!";
	}
}
