package org.handsonknowledge;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class SpringbootMicroserviceControllerTest {
	public SpringbootMicroserviceController controller;
	
	@Before
	public void setup() {
		controller= new SpringbootMicroserviceController();
	}
	
	@Test
	public void returnsHelloWorld() {
		assertThat( controller.helloWorld() ).isEqualTo("Hello world!");
	}
}
